<?php
$servername = "localhost";
$username = "targem";
$password = "targem";

$conn = mysqli_connect($servername, $username, $password);

if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

$sql = 'DROP DATABASE targem';
if ($conn->query($sql) === FALSE) {
    echo "Error dropping database: " . $conn->error;
}

$sql = "CREATE DATABASE targem";
if ($conn->query($sql) === FALSE) {
    echo "Error creating database: " . $conn->error;
}

$sql = "use targem";
if ($conn->query($sql) === FALSE) {
    echo "Error selecting database: " . $conn->error;
}

$sql = "CREATE TABLE Users (
	id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	username VARCHAR(30),
	email VARCHAR(30),
	registrationDate INTEGER,
	status BOOLEAN
)";

if ($conn->query($sql) === FALSE) {
    echo "Error creating table: " . $conn->error;
}


if (($handle = fopen("data.csv", "r")) !== FALSE) {
  while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
  	$Date = new DateTime($data[2]);

  	$username = trim($data[0]);
  	$email = trim($data[1]);
  	$registrationDate = $Date->getTimestamp();
  	$status = trim($data[3]) == 'On'? '1' : '0';

  	$sql = "INSERT INTO Users (username, email, registrationDate, status)
		VALUES ('".$username."', '".$email."', '".$registrationDate."', '".$status."')";
    if ($conn->query($sql) === FALSE) {
        echo "Error inserting User" . $conn->error;
    }
  }
  fclose($handle);
}

$sql = "SELECT username, email, registrationDate, status FROM Users  WHERE status=1 ORDER BY registrationDate ASC";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
    echo "<html><head><meta charset=\"UTF-8\"></meta></head><body><table><tr><th>Ник</th><th>Email</th><th>Зарегистрирован</th><th>Статус</th></tr>";
    while($row = mysqli_fetch_assoc($result)) {
        echo "<tr><td>".$row['username']."</td><td>".$row['email']."</td><td>".date("d.m.Y H:i", $row['registrationDate'])."</td><td>".($row['status'] ? 'On' : 'Off')."</td></tr>";
    }
	echo "</table></body></html>";

} else {
    echo "0 results";
}

$conn->close();
?>